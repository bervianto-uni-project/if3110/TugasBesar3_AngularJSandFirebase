/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package item;

import account.accountObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author raudi
 */
@WebService(serviceName = "Items")
public class Items {
    
    /**
    * Web service operation
     * @param id
     * @return 
    */
    @WebMethod(operationName = "getItemByID")
    @WebResult(name="itemObject")
    public itemObject getItemByID (@WebParam(name = "id") int id) {
        itemObject item = new itemObject();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM item WHERE id = ?";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, id);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            /* Get every data returned by SQL query */
            int i = 0;
            while(rs.next()){
                item = new itemObject( rs.getInt("id"),
                rs.getString("name"),
                rs.getString("description"),
                rs.getInt("price"),
                rs.getString("photo"),
                rs.getInt("seller"),
                rs.getString("date"),
                rs.getString("time")
                );
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return item;
    }
    
    /**
    * Web service operation
     * @param sellerID
     * @return 
    */
    @WebMethod(operationName = "yourProduct")
    @WebResult(name="itemObject")
    public ArrayList<itemObject> yourProduct (@WebParam(name = "sellerID") int sellerID) {
        ArrayList<itemObject> items = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM item WHERE seller = ?";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, sellerID);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            /* Get every data returned by SQL query */
            int i = 0;
            while(rs.next()){
                items.add(new itemObject( rs.getInt("id"),
                rs.getString("name"),
                rs.getString("description"),
                rs.getInt("price"),
                rs.getString("photo"),
                rs.getInt("seller"),
                rs.getString("date"),
                rs.getString("time")
                ));
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
    
    /**
    * Web service operation
     * @return 
    */
    @WebMethod(operationName = "getAllItems")
    @WebResult(name="itemObject")
    public ArrayList<itemObject> getAllItems () {
        ArrayList<itemObject> items = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM item";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            /* Get every data returned by SQL query */
            int i = 0;
            while(rs.next()){
                items.add(new itemObject( rs.getInt("id"),
                rs.getString("name"),
                rs.getString("description"),
                rs.getInt("price"),
                rs.getString("photo"),
                rs.getInt("seller"),
                rs.getString("date"),
                rs.getString("time")
                ));
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
    
    /**
    * Web service operation
     * @param name
     * @return 
    */
    @WebMethod(operationName = "searchItemByName")
    @WebResult(name="itemObject")
    public ArrayList<itemObject> searchItemByName (@WebParam(name = "name") String name) {
        ArrayList<itemObject> items = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM item WHERE name LIKE '%"+name+"%'";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            //dbStatement.setString(1, name);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            /* Get every data returned by SQL query */
            int i = 0;
            while(rs.next()){
                items.add(new itemObject( rs.getInt("id"),
                rs.getString("name"),
                rs.getString("description"),
                rs.getInt("price"),
                rs.getString("photo"),
                rs.getInt("seller"),
                rs.getString("date"),
                rs.getString("time")
                ));
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
    
    /**
    * Web service operation
     * @param name
     * @return 
    */
    @WebMethod(operationName = "searchItemBySeller")
    @WebResult(name="itemObject")
    public ArrayList<itemObject> searchItemBySeller (@WebParam(name = "nameSeller") String nameSeller) {
        String USER_AGENT = "Mozilla/5.0";
        
        String url = "http://localhost:8001/IdentityService/GetUsernameIdFilter";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "nameSeller=" + nameSeller;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        
        ArrayList<itemObject> items = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
            int total = 0;
            JSONObject JSobjek = null;  
            Statement stmt = conn.createStatement();
            
            try {
                JSobjek = new JSONObject(response.toString());
                System.out.println(response.toString());
                total = (int)JSobjek.getInt("total");
                ResultSet rs = null;
                PreparedStatement dbStatement = null;
                for(int i=0;i<total;i++) {
                    
                    dbStatement = conn.prepareStatement("SELECT * FROM item WHERE seller = ?");
                    dbStatement.setInt(1, (int)JSobjek.getInt("id"+i));
                    
                    /* Get every data returned by SQL query */
                    rs = dbStatement.executeQuery();
                    /* Get every data returned by SQL query */
                    while(rs.next()){
                        items.add(new itemObject( rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getInt("price"),
                        rs.getString("photo"),
                        rs.getInt("seller"),
                        rs.getString("date"),
                        rs.getString("time")
                        ));
                    }
                }
                //rs.close();
            } catch (JSONException ex) {
            }
            
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return items;
    }
    
    /**
    * Web service operation
     * @param access_token
     * @param name 
     * @param description 
     * @param price 
     * @param photo 
     * @param date 
     * @param time 
    */
    @WebMethod(operationName = "addProduct")
    @WebResult(name="void")
    public String addProduct (@WebParam(name = "access_token") String access_token,
                            @WebParam(name = "id") int id,
                            @WebParam(name = "userAgent") String userAgent,
                            @WebParam(name = "ip") String ip,
                            @WebParam(name = "name") String name,
                            @WebParam(name = "description") String description,
                            @WebParam(name = "price") double price,
                            @WebParam(name = "photo") String photo,
                            @WebParam(name = "date") String date,
                            @WebParam(name = "time") String time) {
        
        int result = 0;
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        //con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+access_token+"&browser="+userAgent+"&ip_address="+ip;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
        }
        try {
            if (account.getValidate() == 1) {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                        "root",
                        "");

                Statement stmt = conn.createStatement();
                String sql;
                sql = "INSERT into item(name, description, price, photo, seller, date, time) values(?,?,?,?,?,?,?)";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setString(1, name);
                dbStatement.setString(2, description);
                dbStatement.setDouble(3, price);
                dbStatement.setString(4, photo);
                dbStatement.setInt(5, id);
                dbStatement.setString(6, date);
                dbStatement.setString(7, time);
                /* Get every data returned by SQL query */
                int rs = dbStatement.executeUpdate();
                stmt.close();
                conn.close();
                result =  1;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return account.getToken();
    }
    
    /**
    * Web service operation
     * @param access_token
     * @param id
     * @param name 
     * @param description 
     * @param price 
    */
    @WebMethod(operationName = "editProduct")
    @WebResult(name="void")
    public String editProduct (@WebParam(name = "access_token") String access_token,
                            @WebParam(name = "id") int id,
                            @WebParam(name = "userAgent") String userAgent,
                            @WebParam(name = "ip") String ip,
                            @WebParam(name = "name") String name,
                            @WebParam(name = "description") String description,
                            @WebParam(name = "price") double price) {
        
        int result = 0;
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        //con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+access_token+"&browser="+userAgent+"&ip_address="+ip;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
        }
        try {
            if (account.getValidate() == 1) {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                        "root",
                        "");

                Statement stmt = conn.createStatement();
                String sql;
                sql = "UPDATE item SET name=?, description=?, price=? WHERE id=?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setString(1, name);
                dbStatement.setString(2, description);
                dbStatement.setDouble(3, price);
                dbStatement.setInt(4, id);
                /* Get every data returned by SQL query */
                int rs = dbStatement.executeUpdate();
                stmt.close();
                conn.close();
                result = 1;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return account.getToken();
    }
    
    /**
    * Web service operation
     * @param access_token 
     * @param id 
    */
    @WebMethod(operationName = "deleteProduct")
    @WebResult(name="void")
    public String deleteProduct (@WebParam(name = "access_token") String access_token,
                              @WebParam(name = "id") int id,
                              @WebParam(name = "userAgent") String userAgent,
                              @WebParam(name = "ip") String ip) {
        
        int result = 0;
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        //con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+access_token+"&browser="+userAgent+"&ip_address="+ip;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
        }
        try {
            if (account.getValidate() == 1) {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                        "root",
                        "");

                Statement stmt = conn.createStatement();
                String sql;
                sql = "DELETE FROM item WHERE id= ?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setInt(1, id);
                /* Get every data returned by SQL query */
                int rs = dbStatement.executeUpdate();
                stmt.close();
                conn.close();
                result = 1;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return account.getToken();
    }
    
    /**
    * Web service operation
     * @param access_token 
     * @param id 
     * @return  
    */
    @WebMethod(operationName = "countLikes")
    @WebResult(name="Integer")
    public int countLikes (@WebParam(name = "access_token") int access_token,
                           @WebParam(name = "id") int id) {
        int i = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM likes WHERE id_item = ?";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, id);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            while(rs.next()){
                ++i;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    /**
    * Web service operation
     * @param access_token 
     * @param id 
     * @return  
    */
    @WebMethod(operationName = "countPurchases")
    @WebResult(name="Integer")
    public int countPurchases (@WebParam(name = "access_token") int access_token,
                               @WebParam(name = "id") int id) {
        int i = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM buys WHERE id_item = ?";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, id);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            while(rs.next()){
                i += rs.getInt("quantity");
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return i;
    }
    
    /**
    * Web service operation
     * @param access_token
     * @param id
    */
    @WebMethod(operationName = "like")
    @WebResult(name="void")
    public String like (@WebParam(name = "access_token") String access_token,
                     @WebParam(name = "idLiker") int idLiker,
                     @WebParam(name = "id") int id,
                     @WebParam(name = "userAgent") String userAgent,
                     @WebParam(name = "ip") String ip) {
        int result = 0;
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        //con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+access_token+"&browser="+userAgent+"&ip_address="+ip;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
        }
        try {
            if (account.getValidate() == 1) {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                        "root",
                        "");

                Statement stmt = conn.createStatement();
                String sql;
                sql = "SELECT * FROM likes WHERE id_item= ? AND id_account= ? LIMIT 1";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setInt(1, id);
                dbStatement.setInt(2, idLiker);
                /* Get every data returned by SQL query */
                ResultSet rs = dbStatement.executeQuery();
                if (rs.next()){

                } else {
                    String sqllikes;
                    sqllikes = "INSERT INTO likes (id_item,id_account) VALUES (?,?)";
                    PreparedStatement dbStatementLikes = conn.prepareStatement(sqllikes);
                    dbStatementLikes.setInt(1, id);
                    dbStatementLikes.setInt(2, idLiker);
                    int rsLikes = dbStatementLikes.executeUpdate();
                }
                rs.close();
                stmt.close();
                conn.close();
                result = 1;
            } else if (account.getValidate() == 2) {
                result = 2;
            } else if (account.getValidate() == 3) {
                result = 3;
            } else if (account.getValidate() == 4) {
                result = 4;
            } else if (account.getValidate() == 5) {
                result = 5;
            }
            
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex); 
        }
        return account.getToken();
    }
    
    /**
    * Web service operation
     * @param access_token
     * @param id
    */
    @WebMethod(operationName = "dislike")
    @WebResult(name="void")
    public String dislike (@WebParam(name = "access_token") String access_token,
                         @WebParam(name = "id") int id,
                         @WebParam(name = "idDisliker") int idDisliker,
                         @WebParam(name = "userAgent") String userAgent,
                         @WebParam(name = "ip") String ip) {
        
        int result = 0;
        String url = "http://localhost:8001/IdentityService/ValidateToken";
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
        }
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) obj.openConnection();
        } catch (IOException ex) {
            
        }

        try {
            //add reuqest header
            con.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            
        }
        //con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "token="+access_token+"&browser="+userAgent+"&ip_address="+ip;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = null;
        
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
        } catch (IOException ex) {
        }
        
        StringBuffer response = new StringBuffer();
        try {
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
        } catch (IOException ex) {
        }
        System.out.println(response.toString());
        
        accountObject account = null;
        JSONObject JSobjek = null;  
        try {
            JSobjek = new JSONObject(response.toString());
            
            account = new accountObject((int)JSobjek.get("id"), 
                                    (String)JSobjek.get("fullname"),
                                    (String)JSobjek.get("username"),
                                    (String)JSobjek.get("address"),
                                    (String)JSobjek.get("postalcode"),
                                    (String)JSobjek.get("phonenumber"),
                                    (String)JSobjek.get("token"),
                                    (int)JSobjek.get("validate") );
        } catch (JSONException ex) {
        }
        try {
            if (account.getValidate() == 1) {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                        "root",
                        "");

                Statement stmt = conn.createStatement();
                String sql;
                sql = "DELETE FROM likes WHERE id_item = ? and id_account = ?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setInt(1, id);
                dbStatement.setInt(2, idDisliker);
                /* Get every data returned by SQL query */
                int rs = dbStatement.executeUpdate();
                stmt.close();
                conn.close();
                result = 1;
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return account.getToken();
    }
    
    /**
    * Web service operation
     * @param access_token
     * @param id
     * @return 
    */
    @WebMethod(operationName = "checkLike")
    @WebResult(name="boolean")
    public boolean checkLike (@WebParam(name = "access_token") int access_token, 
                       @WebParam(name = "id") int id) {
        boolean check = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Marketplace?zeroDateTimeBehavior=convertToNull",
                    "root",
                    "");
        
            Statement stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM likes WHERE id_item= ? AND id_account= ? LIMIT 1";
            PreparedStatement dbStatement = conn.prepareStatement(sql);
            dbStatement.setInt(1, id);
            dbStatement.setInt(2, access_token);
            /* Get every data returned by SQL query */
            ResultSet rs = dbStatement.executeQuery();
            if (rs.next()){
                check = true;
            }
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Items.class.getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }
}
