/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author raudi
 */
@WebServlet(name = "TokenChat", urlPatterns = {"/TokenChat"})
public class TokenChat extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String username = request.getParameter("username");
            String tokenChat = request.getParameter("tokenChat");
           
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(TokenChat.class.getName()).log(Level.SEVERE, null, ex);
            }
            try
            (   Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/Chat?zeroDateTimeBehavior=convertToNull",
                    "root",
                    ""); 
                Statement stmt = conn.createStatement()) {
                String sql;
                sql = "SELECT * FROM tokenChat WHERE username = ?";
                PreparedStatement dbStatement = conn.prepareStatement(sql);
                dbStatement.setString(1, username);
                JSONObject json = new JSONObject();
                /* Get every data returned by SQL query */
                ResultSet rs = dbStatement.executeQuery();
                if (!rs.next()) {
                    String sqlInputToken;
                    sqlInputToken = "INSERT INTO tokenChat(username,tokenChat) VALUES(?,?)";
                    PreparedStatement dbStatementInputToken = conn.prepareStatement(sqlInputToken);
                    dbStatementInputToken.setString(1, username);
                    dbStatementInputToken.setString(2, tokenChat);
                    int rsInputToken = dbStatementInputToken.executeUpdate();
                    //response.getWriter().write("asd");
                    json.put("success",1);
                } else {
                    json.put("success",0);
                }
                response.setContentType("application/json");
                response.getWriter().write(json.toString());
                rs.close();
                stmt.close();
                conn.close();
            } catch(SQLException s) {
                response.getWriter().write("sql");
            } catch (JSONException ex) {
                Logger.getLogger(TokenChat.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
