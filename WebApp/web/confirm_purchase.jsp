<%-- 
    Document   : confirm_purchase
    Created on : Nov 8, 2016, 10:51:38 PM
    Author     : raudi
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                    
	String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid.getValidate() == 1) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else {
            response.sendRedirect("http://localhost:8000/WebApp/logout.jsp?username="+resultValid.getUsername()+"&validate="+resultValid.getValidate());
            //out.println(resultValid.getUsername());
        }
    %>
	<head> 
		<title>Confirm Purchase</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css"> 
		<script type="text/javascript" src="js/validate.js"></script>
		<script type="text/javascript" src="js/function.js"></script>
	</head> 
	<%-- start web service invocation --%>
        <%
            item.Items_Service service = new item.Items_Service();
            item.Items port = service.getItemsPort();
             // TODO initialize WS operation arguments here
            String strId = request.getParameter("id");
            int id = Integer.parseInt(strId);
            // TODO process result here
            item.ItemObject result = port.getItemByID(id);
            Date dNow = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat ("EEEE',' dd MMMM y");
            SimpleDateFormat timeFormat = new SimpleDateFormat ("HH.mm");
        %>
        <%-- end web service invocation --%>
	<body id="body-color" onload="countTotalPrice(<% out.print(result.getPrice()); %>)"> 
		
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp?username=<%=resultValid.getUsername()%>" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<li><a href="catalog.jsp">Catalog</a></li>
			<li><a href="your_product.jsp">Your Products</a></li>
			<li><a href="add_product.jsp">Add Products</a></li>
			<li><a href="sales.jsp">Sales</a></li>
			<li><a href="purchases.jsp">Purchases</a></li>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> Please confirm your purchase </h2>
		</div>
		    
                        
		<form method="post" id="confirm" action="ConfirmationPurchaseServlet" onsubmit="return validatePurchase();">
                        <input id="accessToken" type="hidden" name="accessToken" value="<%= resultValid.getToken() %>">
                        <input id="userAgent" type="hidden" name="userAgent" value="<%= browser %>">
                        <input id="ip" type="hidden" name="ip" value="<%= ip_address %>"><input id="idSeller" type="hidden" name="idSeller" value="<%=result.getSeller()%>">
                        <input id="idItem" type="hidden" name="idItem" value="<%=result.getId()%>">
                        <input id="idBuyer" type="hidden" name="idBuyer" value="<%=resultValid.getId()%>">
                        <input id="photo" type="hidden" name="photo" value="<%=result.getPhoto()%>">
                    
                        <label>Product : <% out.println(result.getName()); %></label> <br>
			<input id="itemName" type="hidden" name="itemName" value="<%=result.getName()%>">
                        <label>Price : <div id="item-price" name="item-price" class="show"><% out.print(defaultFormat.format(result.getPrice())); %></div></label>  <br>
                        <input id="itemPrice" type="hidden" name="itemPrice" value="<%=result.getPrice()%>">
                        <label for="quantity">Quantity : 
			<input id="quantity" class="quantity" type="text" name="quantity" value="1" onchange="countTotalPrice(<% out.print(result.getPrice()); %>)"> pcs</label> <br>
			<label>Total Price : IDR<div id="total-price" class="show"></div></label> <br>
			<label> Delivery to : </label> <br>
			<br>
			
			<label for="consignee"> Consignee </label> <br>
			<input id="consignee" type="text" name="consignee" value="<%=resultValid.getFullname()%>" onchange="return validateFullName(this.value)"> <br>
			<div id="notif-consignee" class="notif">
			</div>
			
			<label for='address'> Full Address </label><br>
			<textarea id="fullAddress" name="fullAddress"><%=resultValid.getAddress()%></textarea><br>
			<div id="notif-fullAddress" class="notif">
			</div>
			
			<label for="postalcode"> Postal Code </label> <br>
			<input id="postalCode" type="text" name="postalCode" value="<%=resultValid.getPostalcode()%>" onchange="return validatePostalCode(this.value)"> <br>
			<div id="notif-postalCode" class="notif">
			</div>	
			
			<label for="phone"> Phone </label> <br>
			<input id="phoneNumber" type="text" name="phoneNumber" value="<%=resultValid.getPhonenumber()%>" onchange="return validatePhoneNumber(this.value)"> <br>
			<div id="notif-phoneNumber" class="notif">
			</div>
			
			<label for="creditcardnumber"> 12 Digits Credit Card Number </label> <br>
			<input id="ccNumber" type="text" name="ccNumber" onchange="return validateCCNumber()"> <br>
			<div id="notif-ccNumber" class="notif">
			</div>
			
			<label for="cardvalue"> 3 Digits Card Value </label> <br>
			<input id="verificationCode" type="text" name="verificationCode" onchange="return validateCCValue()"> <br>
			<div id="notif-verificationCode" class="notif">
			</div>
			<input type="hidden" id="date" name="date" value="<%=dateFormat.format(dNow) %>">
                        <input type="hidden" id="time" name="time" value="<%=timeFormat.format(dNow) %>">
            
			<div class="submit-style">
				<input id="submit" type="submit" size="20" name="confirm" value="CONFIRM"> 
				<input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='catalog.jsp'"> 
			</div>
		</form>	
	</body> 
        
</html>


