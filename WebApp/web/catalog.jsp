<%-- 
    Document   : catalog
    Created on : Nov 8, 2016, 10:28:46 PM
    Author     : raudi
--%>

<%@page import="org.json.JSONObject"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.xml.ws.ProtocolException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>

<!DOCTYPE HTML>
<html>     
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName()).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
        String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid.getValidate() == 1) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else {
            response.sendRedirect("http://localhost:8000/WebApp/logout.jsp?username="+resultValid.getUsername()+"&validate="+resultValid.getValidate());
            //out.println(resultValid.getUsername());
        }
    %>
    <%-- start web service invocation --%>
    <%
    	account.Accounts_Service serviceAllOnline = new account.Accounts_Service();
	account.Accounts portAllOnline = serviceAllOnline.getAccountsPort();
	// TODO process result here
	java.util.List<java.lang.String> allOnline = portAllOnline.getAllOnlineUsername();
	//out.println("Result = "+allOnline.size());
    %>
    <%-- end web service invocation --%>

	<head> 
		<title>Catalog</title>  
		<link rel="stylesheet" type="text/css" href="css/main.css">
		
		<script type="text/javascript" src="js/function.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	</head> 
	<body ng-app="myApp" ng-controller="myCtrl" ng-init="chats=['asd', 'wew']" id="body-color"> 
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp?username=<%=resultValid.getUsername()%>" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
                    <li><a class="active" href="catalog.jsp">Catalog</a></li>
                    <li><a href="your_product.jsp">Your Products</a></li>
                    <li><a href="add_product.jsp">Add Products</a></li>
                    <li><a href="sales.jsp">Sales</a></li>
                    <li><a href="purchases.jsp">Purchases</a></li>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> What are you going to buy today? </h2>
		</div>
            <form action="catalog.jsp" id="search" method="post">
                    <input type="text" name="search" id="search" class="searchfield" placeholder="Search catalog...">
                    <input class="search" type="submit" name="go" value="GO">
                    <div class="filter">
                            <div class="by">
                                    <span> by</span>
                            </div>
                            <div class="filtersearch">
                                    <input type="radio" id="filter" name="filter" value="product" checked/> product <br>
                                    <input type="radio" id="filter" name="filter" value="store"/> store
                             </div>
                             <br><br>
                    </div>
                </form>
		<%-- start web service invocation --%>
                <%
                    java.util.List<item.ItemObject> result = null;
                    String keyword = request.getParameter("search");
                    String filter = request.getParameter("filter");
                    item.Items_Service service = new item.Items_Service();
                    if (keyword == null) {
                        item.Items port = service.getItemsPort();
                        // TODO process result here
                        result = port.getAllItems();
                    } else if (keyword.equals("")) {
                        item.Items port = service.getItemsPort();
                        // TODO process result here
                        result = port.getAllItems();
                    } else {
                       if (filter.equals("product")) {
                            item.Items port = service.getItemsPort();
                            // TODO process result here
                            result = port.searchItemByName(keyword);
                        } else { 
                            item.Items port = service.getItemsPort();
	                    // TODO process result here
                            //out.println("masuk");
                            result = port.searchItemBySeller(keyword);
                        }
                    }
                    NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                    for(int i=0;i<result.size();i++) {
                
                %>
                <%-- end web service invocation --%>
                
		<div class="filter">
			
			<div class="product-view">
				<%-- start web service invocation --%>
                                <%
                                    account.Accounts_Service serviceAccount = new account.Accounts_Service();
                                    account.Accounts portUsername = serviceAccount.getAccountsPort();
                                     // TODO initialize WS operation arguments here
                                    int idSeller = result.get(i).getSeller();
                                    boolean found = false;
                                    // TODO process result here
                                    java.lang.String resultUsername = portUsername.getUsername(idSeller);
                                    for(int j=0; j<allOnline.size() && !found; j++) {
                                        if (allOnline.get(j).equals(resultUsername)) {
                                            found = true;
                                           // out.println(allOnline.size());
                                        }
                                    }
                                    if (found) {
                                        out.println("<a href=\"\" style=\"text-decoration:none;color:#45a049;\" ng-click=\"openChat('"+resultUsername+"')\">" + resultUsername + "</a>");
                                        //out.println("<input type=\"hidden\" ng-model=\"recipient\" value=\""+resultUsername+"\">");
                                        found = false;
                                    } else {
                                        out.println("<a href=\"\" style=\"text-decoration:none;color:#8B0000;\">" + resultUsername + "</a>");
                                    }
                                %>
                                <%-- end web service invocation --%>
				<br>
				added this on <% out.print(result.get(i).getDate()); %>, at <% out.print(result.get(i).getTime()); %>
			</div>
			<div class="product-view">
				<div class="photo">
					<img src=<% out.print(result.get(i).getPhoto()); %> alt="Mountain View" width="100px" height="100px">
				</div>
				<div class="description">
					<b><% out.print(result.get(i).getName()); %></b> <br>
					<% out.print(defaultFormat.format(result.get(i).getPrice())); %><br>
					<font size="1"> <% out.print(result.get(i).getDescription()); %></font>
				</div>
                                    <div class="detail" id="detail<% out.print("id"); %>">
					
					<div>
						<font size="1"> <%
                                                                    item.Items portCountLikes = service.getItemsPort();
                                                                     // TODO initialize WS operation arguments here
                                                                    int accessTokenCountLikes = resultValid.getId();
                                                                    int idCountLikes = result.get(i).getId();
                                                                    // TODO process result here
                                                                    int resultCountLikes = portCountLikes.countLikes(accessTokenCountLikes, idCountLikes);

                                                                    out.print(resultCountLikes);
                                                                %> likes </font>
					</div>
					<div class="npurchase">
						<font size="1"> <% 
                                                                item.Items portCountPurchases = service.getItemsPort();
                                                                 // TODO initialize WS operation arguments here
                                                                int accessTokenCountPurchases = resultValid.getId();
                                                                int idCountPurchases = result.get(i).getId();
                                                                // TODO process result here
                                                                int resultCountPurchases = portCountPurchases.countPurchases(accessTokenCountPurchases, idCountPurchases);
                                                                out.print(resultCountPurchases); 
                                                                %> purchases </font><br>
					</div>
					<% 
                                            
                                            item.Items portCheck = service.getItemsPort();
                                             // TODO initialize WS operation arguments here
                                            int accessToken = resultValid.getId();
                                            int id = result.get(i).getId();
                                            // TODO process result here
                                            boolean check = portCheck.checkLike(accessToken, id);
                                            
                                            if (check == false) {
                                        %>
                                        <%-- end web service invocation --%>
                                                <div class="like-button">
                                                    <div style="display:inline-block;width:40%;">
                                                        <form method="post" id="like" action="LikeServlet">
                                                            <input id="accessToken" type="hidden" name="accessToken" value="<%=resultValid.getToken() %>">
                                                            <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                            <input id="idLiker" type="hidden" name="idLiker" value="<%= resultValid.getId() %>">
                                                            <input id="userAgent" type="hidden" name="userAgent" value="<%= browser %>">
                                                            <input id="ip" type="hidden" name="ip" value="<%= ip_address %>">
                                                            <input id="search" type="hidden" name="search" value="<%= request.getParameter("search") %>">
                                                            <input id="filter" type="hidden" name="filter" value="<%= request.getParameter("filter") %>">
                                                            <input type="submit" class="like" value="LIKE">	
                                                        </form>
                                                    </div>
                                                </div>
                                        <%  } else { %>
						<div class="like-button">
                                                    <div style="display:inline-block;width:40%;">
                                                        <form method="post" id="dislike" action="DislikeServlet">
                                                            <input id="accessToken" type="hidden" name="accessToken" value="<%=resultValid.getToken() %>">
                                                            <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                            <input id="idDisliker" type="hidden" name="idDisliker" value="<%= resultValid.getId() %>">
                                                            <input id="userAgent" type="hidden" name="userAgent" value="<%= browser %>">
                                                            <input id="ip" type="hidden" name="ip" value="<%= ip_address %>">
                                                            <input id="search" type="hidden" name="search" value="<%= request.getParameter("search") %>">
                                                            <input id="filter" type="hidden" name="filter" value="<%= request.getParameter("filter") %>">
                                                            <input type="submit" class="liked" value="LIKED">	
                                                        </form>
                                                    </div>
						</div>
                                        <%    }  %>
					
					<div class="buy-button">
                                            <div style="display:inline-block;width:40%;">
                                            <form method="post" id="purchaseProduct" action="confirm_purchase.jsp">
                                                <input id="accessToken" type="hidden" name="accessToken" value="<%= resultValid.getId() %>">
                                                <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                <input type="submit" class="buy" value="BUY">	
                                            </form>
                                        </div>	
					</div>
				</div>
			</div>
		</div>
                <% } %>
                <div ng-Show="myVar" ng-Init="myVar=false" id="chat" class="chat">
                    <div id="header-chat" class="header-chat">
                        <span id="recipient-chat" class="recipient"></span>
                        <a href="" class="close-chat" style="text-decoration:none;color:red;" ng-click="myVar=false">x</a>
                    </div>
                    <div id="body-chat" class="body-chat">
                        <div ng-bind-html="x" ng-repeat="x in chats">
                            {{ x }}
                        </div>
                    </div>
                    <div id="input-chat" class="input-chat">
                        
                        <input type="text" ng-model="message" ng-init="message=''">
                        <button class="kirim" ng-click="send('<%=resultValid.getUsername()%>')">Kirim</button>
                        
                    </div>
                </div>
                        
        <%
        String tokenChat = request.getParameter("tokenChat");
        if (tokenChat!=null) {
            String url = "http://localhost:8003/ChatService/TokenChat";
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException ex) {
            }
            HttpURLConnection con = null;
            try {
                con = (HttpURLConnection) obj.openConnection();
            } catch (IOException ex) {

            }

            try {
                //add reuqest header
                con.setRequestMethod("POST");
            } catch (ProtocolException ex) {

            }
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "username="+resultValid.getUsername()+"&tokenChat="+tokenChat;
            //out.println(urlParameters);

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = null;

            try {
                wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();
                //out.println("masuk");
            } catch (IOException ex) {
            }

            StringBuffer responsebuff = new StringBuffer();
            try {
                int responseCode = con.getResponseCode();
                BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                        responsebuff.append(inputLine);
                        //out.println("masuk");
                }
                in.close();
            } catch (IOException ex) {
            }

            JSONObject JSobjek = null;  
            out.println(responsebuff.toString());
            JSobjek = new JSONObject(responsebuff.toString());
            int success = (int)JSobjek.getInt("success");
            if (success == 0) {
                response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp");
            } else {
                response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp");
            }
        }
        %>
        
        <script src="https://www.gstatic.com/firebasejs/3.6.1/firebase.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.js"></script>
        <script>
          // Initialize Firebase
          var config = {
            apiKey: "AIzaSyDwXXDho5ogG5dqqouMLC2tWe_0xkfN9tw",
            authDomain: "test-2c579.firebaseapp.com",
            databaseURL: "https://test-2c579.firebaseio.com",
            storageBucket: "test-2c579.appspot.com",
            messagingSenderId: "222886746942"
          };
          firebase.initializeApp(config);

          const messaging = firebase.messaging();
          messaging.requestPermission()
                  .then(function() {
                      console.log('Have permission');
                      return messaging.getToken();
          })
                  .then(function(token) {
                      console.log(token);
          })
                  .catch(function(err) {
                      console.log('Error occured : ');
          })
          
          messaging.onMessage(function(payload) {
              console.log('onMessage: ', payload)
              var appElement = document.querySelector('[ng-app=myApp]');
              var $scope = angular.element(appElement).scope();
              
              if (document.getElementById("recipient-chat").innerHTML != payload.notification.title) {
                  $scope.chats=[];
              }
              document.getElementById("recipient-chat").innerHTML = payload.notification.title;
              //document.getElementById("body-chat").innerHTML += '<span class="recipient-chat">' +payload.notification.body+'</span><br><br><br>';
              //document.getElementById("opener").innerHTML = '{{myVar=true}}';
              $scope.chats.push('<span class="recipient-chat">' + payload.notification.body +'</span><br><br><br>');
                  
              $scope.$apply(function() {
                  $scope.myVar = true;
              });
          })
          
        var app = angular.module('myApp', ['ngSanitize']);
        var url = "http://localhost:8003/ChatService/SendMessage";

        app.controller('myCtrl', function($scope, $http) {
          //$scope.chats = [];
   
          $scope.openChat = function(uname) {
              if (document.getElementById("recipient-chat").innerHTML!=uname) {
                  //document.getElementById("body-chat").innerHTML = "";
                  $scope.chats = [];
              }
              $scope.recipient = uname;
              $scope.myVar = true;
              document.getElementById("recipient-chat").innerHTML = $scope.recipient;
          }
          $scope.send = function(sender) {
            if ($scope.message != "") {
              var message = $scope.message;
              var uname = document.getElementById("recipient-chat").innerHTML;
              var data = {recipient:uname, body:message, sender:sender};
              var config = {params:data};
              $http.get(url,config).then(function(msg){

              });
              //document.getElementById("body-chat").innerHTML += '<span class="sender-chat">' +$scope.message+'</span><br><br><br>';
              $scope.chats.push('<span class="sender-chat">' + $scope.message +'</span><br><br><br>');
              $scope.message = "";
            }
          }
        });
          
        </script>
        
	</body> 
</html>
