<%-- 
    Document   : your_product
    Created on : Nov 8, 2016, 10:53:32 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<!DOCTYPE HTML> 
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
	String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid.getValidate() == 1) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else {
            response.sendRedirect("http://localhost:8000/WebApp/logout.jsp?username="+resultValid.getUsername()+"&validate="+resultValid.getValidate());
            //out.println(resultValid.getUsername());
        }
    %>
	<head> 
		<title>Your Products</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css"> 
	</head> 
	<body id="body-color"> 
		
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp?username=<%=resultValid.getUsername()%>" style="color:#8a1b14;text-decoration:none;"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<li><a href="catalog.jsp">Catalog</a></li>
			<li><a class="active" href="your_product.jsp">Your Products</a></li>
			<li><a href="add_product.jsp">Add Products</a></li>
			<li><a href="sales.jsp">Sales</a></li>
			<li><a href="purchases.jsp">Purchases</a></li>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> What are you going to sell today? </h2>
		</div>
		<div class="filter">
			    <%-- start web service invocation --%>
                            <%
                                item.Items_Service service = new item.Items_Service();
                                item.Items port = service.getItemsPort();
                                 // TODO initialize WS operation arguments here
                                int sellerID = resultValid.getId();
                                // TODO process result here
                                java.util.List<item.ItemObject> result = port.yourProduct(sellerID);
                                
                                
                                NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                                for(int i=0;i<result.size();i++) {
                            %>
                            <%-- end web service invocation --%>

			<div class="product-view">
				<br>
				<% out.print(result.get(i).getDate()); %> <br> at <% out.print(result.get(i).getTime()); %> 
			</div>
			<div class="product-view">
				<div class="photo">
					<img src='<% out.print(result.get(i).getPhoto()); %>' alt="Mountain View" style="width:100px;height:100px;">
				</div>
				<div class="description">
					<b> <% out.print(result.get(i).getName()); %></b> <br>
					<% out.print(defaultFormat.format(result.get(i).getPrice())); %><br>
					<font size="1">  <% out.print(result.get(i).getDescription()); %></font>
				</div>
				<div class="detail">
					<div style="">
						<font size="1">    <%-- start web service invocation --%>   
                                                        <%
                                                            item.Items portCountLikes = service.getItemsPort();
                                                             // TODO initialize WS operation arguments here
                                                            int accessToken = 1;
                                                            int id = result.get(i).getId();
                                                            // TODO process result here
                                                            int resultCountLikes = portCountLikes.countLikes(accessToken, id);
                                                            out.print(resultCountLikes);
                                                        %>
                                                        <%-- end web service invocation --%> likes </font>
					</div>
					<div style="margin-top:-5px;margin-bottom:20px;">
						<font size="1">
                                                        <%
                                                            item.Items portCountPurchases = service.getItemsPort();
                                                             // TODO initialize WS operation arguments here
                                                            int accessTokenCountPurchases = 1;
                                                            int idCountPurchases = result.get(i).getId();
                                                            // TODO process result here
                                                            int resultCountPurchases = portCountPurchases.countPurchases(accessTokenCountPurchases, idCountPurchases);
                                                            out.print(resultCountPurchases);
                                                        %>
                                                        <%-- end web service invocation --%>
                                                     purchases </font><br>
					</div>
					<div style="display:inline-block;width:40%;">
                                            <form method="post" id="editProduct" action="edit_product.jsp">
                                                <input id="accessToken" type="hidden" name="accessToken" value="1">
                                                <input id="id" type="hidden" name="id" value="<%= result.get(i).getId() %>">
                                                <input type="submit" class="edit" value="EDIT">	
                                            </form>
                                        </div>

					<div style="display:inline-block;width:40%;">
                                            <form method="post" id="deleteProduct" action="DeleteProductServlet">
                                                <input id="accessToken" type="hidden" name="accessToken" value="<%=resultValid.getToken() %>">
                                                <input id="userAgent" type="hidden" name="userAgent" value="<%= browser %>">
                                                <input id="ip" type="hidden" name="ip" value="<%= ip_address %>">
                                                <input id="id" type="hidden" name="id" value="<%=result.get(i).getId()%>">
                                                <input type="submit" class="delete" value="DELETE">	
                                            </form>
                                        </div>
				</div>
			</div>
                        <% } %>
		</div>	
		
	</body>
</html>
