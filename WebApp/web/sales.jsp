<%-- 
    Document   : sales
    Created on : Nov 8, 2016, 10:53:08 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<!DOCTYPE HTML>
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
	String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid.getValidate() == 1) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else {
            response.sendRedirect("http://localhost:8000/WebApp/logout.jsp?username="+resultValid.getUsername()+"&validate="+resultValid.getValidate());
            //out.println(resultValid.getUsername());
        }
    %>
	<head> 
		<title>Sales</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css"> 
	</head> 
	<body id="body-color"> 
		
		<div>
			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp?username=<%=resultValid.getUsername()%>" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<li><a href="catalog.jsp">Catalog</a></li>
			<li><a href="your_product.jsp">Your Products</a></li>
			<li><a href="add_product.jsp">Add Products</a></li>
			<li><a class="active" href="sales.jsp">Sales</a></li>
			<li><a href="purchases.jsp">Purchases</a></li>
		</ul>
		<br><br>
		</div>
		<div>
			<h2> Here are your sales </h2>
		</div>
		    <%-- start web service invocation --%>
                    <%
                        buys.Buys_Service service = new buys.Buys_Service();
                        buys.Buys port = service.getBuysPort();
                         // TODO initialize WS operation arguments here
                        int sellerID = resultValid.getId();
                        // TODO process result here
                        java.util.List<buys.BuysObject> result = port.sales(sellerID);
                        
                        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
                        for(int i=0;i<result.size();i++) {
                    %>
                    <%-- end web service invocation --%>

		<div class="filter">
			<div class="product-view">
				<% out.print(result.get(i).getDate()); %> <br> at <% out.print(result.get(i).getTime()); %>
			</div>
			<div class="product-view">
				<div class="photo">
					<img src=<% out.print(result.get(i).getPhoto()); %>  alt="Mountain View" width="100px" height="100px">
				</div>
				<div class="description-sales">
					<b><% out.print(result.get(i).getItemName()); %> </b> <br>
					<% out.print(defaultFormat.format(result.get(i).getQuantity()*result.get(i).getItemPrice()));%><br>
					<% out.print(result.get(i).getQuantity()); %> pcs<br>
					@<% out.print(defaultFormat.format(result.get(i).getItemPrice())); %><br>
					<br>
                                        <font size="1">bought by <b>
                                            <%-- start web service invocation --%>
                                            <%
                                                account.Accounts portUsername = serviceValid.getAccountsPort();
                                                 // TODO initialize WS operation arguments here
                                                int id = result.get(i).getIdBuyer();
                                                // TODO process result here
                                                java.lang.String resultUsername = portUsername.getUsername(id);
                                                out.println(resultUsername);
                                            %> </b>
                                            <%-- end web service invocation --%>
                                        </font>
				</div>
				<div class="detail-sales">
					Delivery to <b><% out.print(result.get(i).getConsignee()); %></b><br>
					<% out.print(result.get(i).getFullAddress()); %><br>
					<% out.print(result.get(i).getPostalCode()); %><br>
					<% out.print(result.get(i).getPhoneNumber()); %><br>
				</div>
			</div>
		</div>
		<% } %>
		</form>	
	</body> 
</html>
