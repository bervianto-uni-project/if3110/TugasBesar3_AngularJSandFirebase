<%-- 
    Document   : edit_product
    Created on : Nov 8, 2016, 10:52:14 PM
    Author     : raudi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html> 
<html> 
    <%-- start web service invocation --%>
    <%
	account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookie = cookies[i];
            if((cookie.getName( )).compareTo("token") == 0 ){
               token = cookie.getValue();
               //cookie.setMaxAge(0);
            }
        }
	//java.lang.String token = "qwertyuiop";
	// TODO process result here
	String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String  user = userAgent.toLowerCase();

        String os = "";
        String browser = "";



        //=================OS=======================
         if (userAgent.toLowerCase().indexOf("windows") >= 0 )
         {
             os = "Windows";
         } else if(userAgent.toLowerCase().indexOf("mac") >= 0)
         {
             os = "Mac";
         } else if(userAgent.toLowerCase().indexOf("x11") >= 0)
         {
             os = "Unix";
         } else if(userAgent.toLowerCase().indexOf("android") >= 0)
         {
             os = "Android";
         } else if(userAgent.toLowerCase().indexOf("iphone") >= 0)
         {
             os = "IPhone";
         }else{
             os = "UnKnown, More-Info: "+userAgent;
         }
         //===============Browser===========================
        if (user.contains("msie"))
        {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera"))
        {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) )
        {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";

        } else if (user.contains("firefox"))
        {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv"))
        {
            browser="IE";
        } else
        {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid.getValidate() == 1) {   
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",resultValid.getToken());
            response.addCookie(cookieExpires);
        } else {
            response.sendRedirect("http://localhost:8000/WebApp/logout.jsp?username="+resultValid.getUsername()+"&validate="+resultValid.getValidate());
            //out.println(resultValid.getUsername());
        }
    %>
	<head> 
		<title>Edit Product</title> 
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script type="text/javascript" src="js/validate.js"></script>
	</head> 
	<body id="body-color"> 
		
 		<div>
 			<h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
 		</div>
		<div class="atas">
			Hi, <%out.println(resultValid.getUsername());%>!<br>
			<a href="logout.jsp?username=<%=resultValid.getUsername()%>" class="logout"><font size="1">logout</font></a>
		</div>
		<div class="navbar">
		<ul>
			<li><a href="catalog.jsp">Catalog</a></li>
 			<li><a href="your_product.jsp">Your Products</a></li>
 			<li><a href="add_product.jsp">Add Products</a></li>
 			<li><a href="sales.jsp">Sales</a></li>
			<li><a href="purchases.jsp">Purchases</a></li>
 		</ul>
		</div>
		<br>
		<div>
			<h2> Please update your product here </h2>
		</div>
                    <%-- start web service invocation --%>
                    <%
                        item.Items_Service service = new item.Items_Service();
                        item.Items port = service.getItemsPort();
                         // TODO initialize WS operation arguments here
                        String strId = request.getParameter("id");
                        int id = Integer.parseInt(strId);
                        // TODO process result here
                        item.ItemObject result = port.getItemByID(id);
                    
                    %>
                    <%-- end web service invocation --%>

		<form method="post" id="edit_product" action="EditProductServlet">
                    <input id="accessToken" type="hidden" name="accessToken" value="<%=resultValid.getToken() %>">
                    <input id="userAgent" type="hidden" name="userAgent" value="<%= browser %>">
                    <input id="ip" type="hidden" name="ip" value="<%= ip_address %>">
                    <input type="hidden" name="id" id="id" value="<%=result.getId()%>">
			<label for="product_name">Name</label><br>
                        <input type="text" name="name" id="name" value="<% out.print(result.getName()); %>"><br>
			<div id="notif-name" class="notif">
			</div>
			<label for="product_desc">Description (max 200 chars)</label><br>
			<textarea rows="5" cols="107" name="description" id="description"><% out.print(result.getDescription()); %></textarea><br>
			<div id="notif-description" class="notif">
			</div>
			<label for="product_price">Price (IDR)</label><br>
			<input type="text" name="price" id="price" value="<% out.print(result.getPrice()); %>"><br>
			<div id="notif-price" class="notif">
			</div>
			<label for="product_photo">Photo</label><br>
			<input type="file" accept="image/*" align="middle" name="photo" id="photo" disabled><br>
			<div class="notif" id="notif">
				 <p>* Sorry, you can't edit your product photo.</p>
			</div>
			<div class="submit-style">
				<input id="button" type="submit" name="edit_product" value="UPDATE"> 
				<input id="cancel" class="cancel" type="button" size="20" value="CANCEL" onclick="window.location.href='your_product.jsp'"> 
			</div>
		</form>	
	</body> 
</html>


