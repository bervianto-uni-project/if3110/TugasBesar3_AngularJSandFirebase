<%-- 
    Document   : index
    Created on : Nov 6, 2016, 3:02:22 PM
    Author     : raudi
--%>

<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.xml.ws.ProtocolException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.URL"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML> 
<html> 	
    <% 
        String warning = new String();
    
        account.Accounts_Service serviceValid = new account.Accounts_Service();
	account.Accounts portValid = serviceValid.getAccountsPort();
	String token = null;
        Cookie cookiecek = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            cookiecek = cookies[i];
            if((cookiecek.getName()).compareTo("token") == 0 ){
               token = cookiecek.getValue();
               //cookie.setMaxAge(0);
            }
        }
        
        String userAgent = request.getHeader("User-Agent");
        String test = userAgent;
        String user = userAgent.toLowerCase();

        String os = "";
        String browser = "";

        //=================OS=======================
        if (userAgent.toLowerCase().indexOf("windows") >= 0 ) {
            os = "Windows";
        } else if(userAgent.toLowerCase().indexOf("mac") >= 0) {
           os = "Mac";
        } else if(userAgent.toLowerCase().indexOf("x11") >= 0) {
            os = "Unix";
        } else if(userAgent.toLowerCase().indexOf("android") >= 0) {
            os = "Android";
        } else if(userAgent.toLowerCase().indexOf("iphone") >= 0) {
            os = "IPhone";
        } else {
            os = "UnKnown, More-Info: "+userAgent;
        }

        //===============Browser===========================
        if (user.contains("msie")) {
            String substring=userAgent.substring(userAgent.indexOf("MSIE")).split(";")[0];
            browser=substring.split(" ")[0].replace("MSIE", "IE")+"-"+substring.split(" ")[1];
        } else if (user.contains("safari") && user.contains("version")) {
            browser=(userAgent.substring(userAgent.indexOf("Safari")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
        } else if ( user.contains("opr") || user.contains("opera")) {
            if(user.contains("opera"))
                browser=(userAgent.substring(userAgent.indexOf("Opera")).split(" ")[0]).split("/")[0]+"-"+(userAgent.substring(userAgent.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if(user.contains("opr"))
                browser=((userAgent.substring(userAgent.indexOf("OPR")).split(" ")[0]).replace("/", "-")).replace("OPR", "Opera");
        } else if (user.contains("chrome")) {
            browser=(userAgent.substring(userAgent.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        } else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)  || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1) || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1) ) {
            //browser=(userAgent.substring(userAgent.indexOf("MSIE")).split(" ")[0]).replace("/", "-");
            browser = "Netscape-?";
        } else if (user.contains("firefox")) {
            browser=(userAgent.substring(userAgent.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        } else if(user.contains("rv")) {
            browser="IE";
        } else {
            browser = "UnKnown, More-Info: "+userAgent;
        }
        String ip_address = request.getRemoteAddr();
        account.AccountObject resultValid = portValid.validate(token,browser,ip_address);
        if (resultValid == null) {
            warning = "Can't validate";
            warning += token;
            warning += ip_address;
            warning += browser;
            out.println(warning);
        } else {
            // TODO process result here
            if (resultValid.getValidate() == 1) {
                response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp");
            }


            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String email = request.getParameter("email");

            //String username = "bebas";
            //String password = "bebas";

            String url = "http://localhost:8001/IdentityService/SignIn";
            URL obj = null;
            try {
                obj = new URL(url);
            } catch (MalformedURLException ex) {
            }
            HttpURLConnection con = null;
            try {
                con = (HttpURLConnection) obj.openConnection();
            } catch (IOException ex) {

            }

            try {
                //add reuqest header
                con.setRequestMethod("POST");
            } catch (ProtocolException ex) {

            }
            con.setRequestProperty("User-Agent", browser);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                
            String urlParameters = "username="+username+"&password="+password+"&email="+email+"&ip_address="+ip_address;

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = null;

            try {
                wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();
            } catch (IOException ex) {
            }

            StringBuffer responsebuff = new StringBuffer();
            try {
                int responseCode = con.getResponseCode();
                BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
                String inputLine;

                while ((inputLine = in.readLine()) != null) {
                        responsebuff.append(inputLine);
                }
                in.close();
            } catch (IOException ex) {
            }

            JSONObject JSobjek = null;  
            //out.println("test"+responsebuff.toString());
            JSobjek = new JSONObject(responsebuff.toString());

            //str = (String)JSobjek.get("id");
            int respId = (int)JSobjek.getInt("id");
            //if (!str.equals("null")) {
            //respId = Integer.parseInt(str);
            //}
            if (respId == 0) {
                //response.sendRedirect("http://localhost:8000/WebApp/index.jsp");
                warning = "Failed Login";
            } else {
                Cookie cookie = new Cookie("token",(String)JSobjek.getString("token"));
                response.addCookie(cookie);
                String tokenChat = request.getParameter("tokenChat");
                response.sendRedirect("http://localhost:8000/WebApp/catalog.jsp?tokenChat=" + tokenChat);
            }
        }
    %>
    <head> 
	<title>Sign-In</title> 
	<link rel="stylesheet" type="text/css" href="css/main.css"> 
	<script type="text/javascript" src="js/validate.js"></script>
        <script src="https://www.gstatic.com/firebasejs/3.6.1/firebase.js"></script>
        <script>
          // Initialize Firebase
          var config = {
            apiKey: "AIzaSyDwXXDho5ogG5dqqouMLC2tWe_0xkfN9tw",
            authDomain: "test-2c579.firebaseapp.com",
            databaseURL: "https://test-2c579.firebaseio.com",
            storageBucket: "test-2c579.appspot.com",
            messagingSenderId: "222886746942"
          };
          firebase.initializeApp(config);

          const messaging = firebase.messaging();
          messaging.requestPermission()
                  .then(function() {
                      console.log('Have permission');
                      return messaging.getToken();
          })
                  .then(function(token) {
                      console.log(token);
                      document.getElementById("tokenChat").value=token;
          })
                  .catch(function(err) {
                      console.log('Error occured : ');
          })

          messaging.onMessage(function(payload) {
              console.log('onMessage: ', payload)
          })
        </script>
    </head> 
    <body id="body-color"> 
        <%
            if (request.getParameter("validate")!=null) {
                if (request.getParameter("validate").equals("2")) {
                    out.println("Token Expired");
                } else if (request.getParameter("validate").equals("3")) {
                    out.println("Browser Invalid");
                } else if (request.getParameter("validate").equals("4")) {
                    out.println("IP Address Invalid");
                } else if (request.getParameter("validate").equals("5")) {
                    out.println("Token Invalid");
                } else {
                    out.println(request.getParameter("validate"));
                }
            } else {
                if (resultValid.getValidate() == 2) {
                    out.println("Token Expired");
                } else if (resultValid.getValidate() == 3) {
                    out.println("Browser Invalid");
                } else if (resultValid.getValidate() == 4) {
                    out.println("IP Address Invalid");
                } else if (resultValid.getValidate() == 5) {
                    out.println("Token Invalid");
                } else {
                    //out.println(request.getParameter("validate"));
                }
            }
        %>
        <div>
            <h1> <font color="#8a1b14">Sale</font><font color="#6e95e4">Project</font> </h1>
	</div>
	<div>
            <h2> Please login </h2>
	</div>
		
	<form method="POST" action="index.jsp"> 
            <label for="fname">Email or Username</label><br>
            <input type="text" id="username" name="username" ><br>
            <div id="notif-username" class="notif">
            </div>
            <label for="fname">Password </label><br>
            <input type="password" id="password" name="password" ><br>
            <div id="notif-password" class="notif">
            </div>
            <input type="hidden" id="tokenChat" name="tokenChat"/>
            <div class="submit-style">
                <input id="button" type="submit" name="login" value="LOGIN"> 
            </div>
	</form>
	<br><br><br>
	<div class="register"> 
            <b>Dont have an account yet? Register <a class="link-register" href="register.jsp">here</a></b>
	</div>		
    </body> 
</html>
