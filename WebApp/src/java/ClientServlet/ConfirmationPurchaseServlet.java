/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClientServlet;

import buys.Buys_Service;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author raudi
 */
@WebServlet(name = "ConfirmationPurchaseServlet", urlPatterns = {"/ConfirmationPurchaseServlet"})
public class ConfirmationPurchaseServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8002/MarketplaceWebService/Buys.wsdl")
    private Buys_Service service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String strIdItem = request.getParameter("idItem");
            int idItem = Integer.parseInt(strIdItem);
            String strIdBuyer = request.getParameter("idBuyer");
            int idBuyer = Integer.parseInt(strIdBuyer);
            String strIdSeller =request.getParameter("idSeller");
            int idSeller = Integer.parseInt(strIdSeller);
            java.lang.String photo = request.getParameter("photo");   
            java.lang.String itemName = request.getParameter("itemName");
            String strPrice = request.getParameter("itemPrice");
            int itemPrice = Integer.parseInt(strPrice);
            String strQuantity = request.getParameter("quantity");
            int quantity = Integer.parseInt(strQuantity);
            String consignee = request.getParameter("consignee");
            String fullAddress = request.getParameter("fullAddress");
            String postalCode = request.getParameter("postalCode");
            String phoneNumber = request.getParameter("phoneNumber");
            String ccNumber = request.getParameter("ccNumber");
            String verificationCode = request.getParameter("verificationCode");
            String date = request.getParameter("date");
            String time = request.getParameter("time");
            String accessToken = request.getParameter("accessToken");
            String userAgent = request.getParameter("userAgent");
            String ip = request.getParameter("ip");
            
            String newToken = confirmPurchase(accessToken, userAgent, ip, idItem, idBuyer, idSeller, photo, itemName, itemPrice, quantity, consignee, fullAddress, postalCode, phoneNumber, ccNumber, verificationCode, date, time);
            String token = null;
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   //cookie.setMaxAge(0);
                }
            }
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if((cookie.getName()).compareTo("token") == 0 ){
                   token = cookie.getValue();
                   cookie.setMaxAge(0);
                }
            }
            Cookie cookieExpires = new Cookie("token",newToken);
            response.addCookie(cookieExpires);
            response.sendRedirect("purchases.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String confirmPurchase(java.lang.String accessToken, java.lang.String userAgent, java.lang.String ip, int idItem, int idBuyer, int idSeller, java.lang.String photo, java.lang.String itemName, int itemPrice, int quantity, java.lang.String consignee, java.lang.String fullAddress, java.lang.String postalCode, java.lang.String phoneNumber, java.lang.String ccNumber, java.lang.String verificationCode, java.lang.String date, java.lang.String time) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        buys.Buys port = service.getBuysPort();
        return port.confirmPurchase(accessToken, userAgent, ip, idItem, idBuyer, idSeller, photo, itemName, itemPrice, quantity, consignee, fullAddress, postalCode, phoneNumber, ccNumber, verificationCode, date, time);
    }
    
}
